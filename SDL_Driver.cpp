#include <iostream>
#include <SDL2/SDL.h>
#include "include/SDL_Driver.h"
#include <SDL2/SDL_ttf.h>
#include <sstream>
#include <time.h>

using namespace std;

/*Governs how fast the engine can move per frame. 16ms would limit the engine
to 60fps and 8ms would limit the engine to 120fps.*/
const int TICK_INTERVAL = 4;

static Uint32 next_time;

double currTime = 0; //time of current frame
double oldTime = 0; //time of previous frame

double frameTime; //frameTime is the time this frame has taken, in seconds

TTF_Font* sans_font;

////////////////////////////////////////////////////////////////////////////////
//KEYBOARD FUNCTIONS////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool SDL_Driver::keyDown(int key) //this checks if the key is held down, returns true all the time until the key is up
{
  return (inkeys[key] != 0);
}

bool SDL_Driver::keyPressed(int key) //this checks if the key is *just* pressed, returns true only once until the key is up again
{
  if(keypressed.find(key) == keypressed.end()) keypressed[key] = false;
  if(inkeys[key])
  {
    if(keypressed[key] == false)
    {
      keypressed[key] = true;
      return true;
    }
  }
  else keypressed[key] = false;

  return false;
}

///// End of KEYBOARD FUNCTIONS ////

int SDL_Driver::setupSDLRenderer(int WINDOW_WIDTH, int WINDOW_HEIGHT, uint32_t* (*generateRaster)(KeyboardState, double)){
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    bool run = true;
    KeyboardState keyboardState;
    uint32_t color = 0;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
    window = SDL_CreateWindow("Raycaster Engine", 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    // https://gamedev.stackexchange.com/questions/157604/how-to-get-access-to-framebuffer-as-a-uint32-t-in-sdl2
    SDL_Texture* framebuffer = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, WINDOW_WIDTH, WINDOW_HEIGHT);
    uint32_t* pixels = new uint32_t[WINDOW_WIDTH*WINDOW_HEIGHT];  

    if(TTF_Init()==-1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        exit(2);
    }
    sans_font = TTF_OpenFont("font/Sans.ttf", 12); //this opens a font style and sets a size
    if(!sans_font) {
        printf("TTF_OpenFont: %s\n", TTF_GetError());
    }

    SDL_Rect message_rect; //create a rect
    message_rect.x = 0;  //controls the rect's x coordinate 
    message_rect.y = 0; // controls the rect's y coordinte
    message_rect.w = 20; // controls the width of the rect
    message_rect.h = 20; // controls the height of the rect
    SDL_Texture* message;

    next_time = SDL_GetTicks() + TICK_INTERVAL;
    while (run) {
        //Start of future render function
        SDL_RenderClear(renderer);

        rasterPixels = (*generateRaster)(keyboardState, frameTime);

        SDL_UpdateTexture(framebuffer , NULL, rasterPixels, WINDOW_WIDTH * sizeof (uint32_t));
        SDL_RenderCopy(renderer, framebuffer , NULL, NULL);
        // printf("time_left: %d", time_left());
        SDL_Delay(time_left()); //remove this to allow for more than 60 iterations a second
        next_time = SDL_GetTicks() + TICK_INTERVAL;

        //timing for input and FPS counter
        oldTime = currTime;
        currTime = SDL_GetTicks();
        frameTime = (currTime - oldTime) * 0.001; //frameTime is the time this frame has taken, in seconds
        SDL_Color White = {255, 255, 255};  // this is the color in rgb format, maxing out all would give you the color white, and it will be your text's color
        std::stringstream ss;
        ss << (int)(1.0 / frameTime);
        const char* str = ss.str().c_str();
        SDL_Surface* surfaceMessage = TTF_RenderText_Solid(sans_font, str, White); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
        message = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

        //Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes
        SDL_RenderCopy(renderer, message, NULL, &message_rect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture

        //Don't forget too free your surface and texture
        SDL_RenderPresent(renderer);

        while (SDL_PollEvent(&event)) {
            keyboardState.reset();

            //Start of handleInput function
            if (event.type == SDL_QUIT) {
                run = false;
            }
            inkeys = SDL_GetKeyboardState(NULL);
            if (keyDown(SDL_SCANCODE_UP) || keyDown(SDL_SCANCODE_W)) {
                keyboardState.keyUp = true;
            } else if (keyDown(SDL_SCANCODE_DOWN) || keyDown(SDL_SCANCODE_S)) {
                keyboardState.keyDown = true;
            } else if (keyDown(SDL_SCANCODE_LEFT) || keyDown(SDL_SCANCODE_A)) {
                keyboardState.keyLeft = true;
            } else if (keyDown(SDL_SCANCODE_RIGHT) || keyDown(SDL_SCANCODE_D)) {
                keyboardState.keyRight = true;
            }
            //End of handleInput function
        }
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}

int SDL_Driver::time_left(void)
{
    Uint32 now;

    now = SDL_GetTicks();

    //rendered too slow, don't wait
    if(next_time < now) {
        return 0;
    }
    else {
        // rendered too fast, wait until the next tick amount
        return next_time - now;
    }
}

void KeyboardState::reset(){
      keyUp = false;
      keyDown = false;
      keyLeft = false;
      keyRight = false;
  }