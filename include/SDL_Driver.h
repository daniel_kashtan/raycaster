#include <vector>
#include <map>
#include <SDL2/SDL.h>

using namespace std;

struct KeyboardState {
  bool keyUp;
  bool keyDown;
  bool keyLeft;
  bool keyRight;

  void reset();
};

class SDL_Driver {
    private:
        uint32_t* rasterPixels;
        bool keyDown(int key);
        map<int, bool> keypressed; //for the "keyPressed" function to detect a keypress only once
        const Uint8 *inkeys;
        int time_left();
    public:
        int setupSDLRenderer(int WINDOW_WIDTH, int WINDOW_HEIGHT, uint32_t* (*generateRaster)(KeyboardState, double));
        bool keyPressed(int key);
};