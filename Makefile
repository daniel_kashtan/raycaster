all: windows linux

linux:
	g++ RaycasterEngine.cpp -O SDL_Driver.cpp -lSDL2 -lSDL2_ttf -lSDL2_image -o rayCaster_Linux.exe

windows:
	i686-w64-mingw32-g++ -O RaycasterEngine.cpp SDL_Driver.cpp -I/usr/local/i686-w64-mingw32/include -L/usr/local/i686-w64-mingw32/lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_ttf -lSDL2_image -static-libgcc -static-libstdc++ -Wl,-Bstatic -lstdc++ -lpthread -Wl,-Bdynamic -o rayCaster_Win.exe

clean:
	rm rayCaster_Linux.exe rayCaster_Win.exe
